#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
A MODIFIER

REMARQUE:
les variables on ne peut pas les modifier en fesant comme ca self.x() + dx il faut faire:
x=self.x() 
x+dx
Sinon on a cette erreur TypeError: 'int' object is not callable

"""


"""
1. Points du plan

on a importer la librairie math
"""
import math as m

class Point:
    "Un point doit avoir deux attributs num´eriques x, y"
    def __init__(self, x, y):
        #ici on les declare avec des __ devant pour eviter confusion avec les methode declarer apres
        self.x = m.sqrt(x **2) + m.sqrt(y **2)
        self.y = m.atan2(x,y)
       
    def x(self):
        """
            renvoient les coordonnées cartésiennes du point x
            C'est un getter meme principe pour y
        """
   
        return self.x
   
    def y(self):
   
        return self.y
   
    def r(self):
        """
            on va utiler les getter defini avant comme ca on va pas utiliser directement
            les variable du construteur d'ou l'interet des getter
            coordonne polaire
        """
        return m.sqrt(self.x **2) + m.sqrt(self.y **2)
   
    def t(self):
        """
        coordonne polaire
        """

        
        return m.atan2(self.x,self.y)
   
    def __str__(self):
        return "({},{})".format(str(self.x),str(self.y))
     
       
    def __eq__(self,another_point):
        """
            self et another point sont des objets
            a et b représentent-ils deux points égaux ?
        """
        
        return 'ordonnee',self.y == another_point.y, 'abscisse',self.x ==  another_point.x
        
        

    def homothetie(self,k):
        """
            applique au point une homothétie de centre (0, 0) et de rapport k (k est un
            flottant) ; pour cela, il suffit de remplacer (x, y) par (k × x,k × y)
        """
        x = self.x * k 
        y = self.y * k
        return "homothetie : ({},{})".format(str(x),str(y))
       
    def translation(self,dx,dy):
        """
            applique au point une translation de vecteur (dx, dy) ; cela consiste à
            remplacer (x, y) par (x + dx, y + dy).
        """

        x = self.x + dx
        y = self.y + dy
        return "translation : ({},{})".format(str(x),str(y))

       
    def rotation(self,a):
        """  
            applique au point une rotation de centre (0, 0) et d’angle a
        """
        #variable local
        t=self.t()
        r=self.r() 
        
        x = r*(m.cos(t)) + a 
        y = r*(m.sin(t)) +a
        return "rotation : ({},{})".format(str(x),str(y))


   
       

#~ point_un = Point(2,8)


#~ print(point_un.r())
#~ print(point_un.t())
#~ print(point_un.__str__()) 

#~ point_deux =Point(0,0)

#~ print(point_un.__eq__(point_deux))  
#~ print(point_un.homothetie(2))    
#~ print(point_un.translation(1,2))  
#~ print(point_un.rotation(2))









       


  

