class Pile:
	""" LIFO Last in First out """
	
	def __init__(self):
		""" Create a new Pile"""
		self.p = []
		
		
	def estVide(self):
		return self.p == []
		
	def empiler(self,e):
		""" add new element on a pile """
		self.p.append(e)
	
	def depiler(self):
		"""remove element on a pile  """
		
		#read an empty pile (lecture d'une pile vide)
		#self.method use to call a method on the same class
		if self.estVide():
				raise IndexError("attempt to pop from empty stack")
		
		#remove the last element on the list 
		return self.p.pop()
	
	def sommet(self):
		
		#try to read a pile by the top (we read pile by the end)
		if self.estVide():
				raise IndexError("attempt to read top of empty stack")
		#if it's not empty return the last value of the list
		return self.p[-1]
		
		


class File:
    """FIFO fist in Fist out """
    
    def __init__(self):
		""" Create a new File """
		self.f=[]
		
	def estVide(self):
		return self.f == []
		
	def enfiler(self,e):
		""" add new element on a file """
		self.f.append(e)
	
	def defiler(self):
		"""remove element on a file  """
		
		if self.estVide():
			raise IndexError("attempt to remove element from empty queue")
		
		#remove from the top 
		return self.f.pop(0)
		
	def tete(f):
		
		#try to read a file but it's empty
		if self.estVide():
			raise IndexError("attempt to read head of empty queue")
		#if it's not empty return the first value ( the one which will be remove next if we use defiler)
		return f[0]

    
   


if __name__ == "__main__": # ne pas toucher au code ci-dessous
    f = File()
    for i in range(5):
        f.enfiler(i)
    while not f.estVide():
        print(f.defiler())
    try:
        f.defiler()
    except IndexError as e:
        print(e)
    try:
        print(f.tete())
    except IndexError as e:
        print(e)

    p = Pile()
    for i in range(5):
        p.empiler(i)
    while not p.estVide():
        print(p.depiler())
    try:
        p.depiler()
    except IndexError as e:
        print(e)
    try:
        print(p.sommet())
    except IndexError as e:
        print(e)


