#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 15:33:18 2020

@author: c19012324

faire des commentaire triple guillement pour la doc

"""
import math as m

#import class_point_v2 as p
import class_point as p

    
        
class LignePol:
    """
        Application de la class point
        Une ligne polygonale est définie par un certain nombre
        de points, ses sommets
    """
    def __init__(self, sommets):
        #La liste des points qui sont les sommets de la ligne polygonale
        self._sommets =sommets #pour savoir que sommet est une liste de point
        #le nombre de sommets (ici, cette information est redondante
        self._nb_sommets = len(sommets)
   
   
    def get_sommet(self, i):
        """
            renvoie le ième sommet
        """
        return self._sommets[i]
   
   
    def set_sommet(self, i, p):
        """
            remplace la ieme valeur par p
        """
        self._sommets[i] = p
        return self._sommets
       

    def __str__(self):

       
       text=""

       for i in range (0,2):
              text =  text + str(self._sommets[i])        
      
       return text

           
    def homothetie(self,k):
        """
            applique à chaque sommet de la ligne polygonale une homothétie de
            centre (0, 0) et de rapport k,
           
            on modifie la liste sommets directement sans cree une new liste
        """
        sommet_list=[]
        for i in range(self._nb_sommets):
                sommet=(str(self.get_sommet(i)))
                #print(int(sommet[3])*k)
                sommet_list.append(int(sommet[1])*k)
                sommet_list.append( sommet[3]*k)
         
        return sommet_list                
                        
            
       
    def translation(self,dx,dy):
        translation=[]
        s=''
        for i in range(self._nb_sommets):
                
                sommet=str(self.get_sommet(i))
				
                translation.append(int(sommet[1])+dx)
                translation.append(int(sommet[3]) +dy)
        return translation
       
       
       
    def rotation(self, a):
        rotation=[]
        for i in range(self._nb_sommets):
                sommet=str(self.get_sommet(i))
                
                t=m.atan2(int(sommet[3]),int(sommet[1]))
                r=m.sqrt(int(sommet[3]) ** 2 + int(sommet[1]) ** 2)
                rotation.append(r*(m.cos(t+a)))
                rotation.append(r*(m.sin(t+a)))
                
        return rotation
 
   
    def tracer(self):
        """
            produit le tracé de la ligne polygonale
        """
       
        #sommet liste de points ? defini dedans le x et le y ... ?
       
       
       #print ( "tracer de (" + str(self.__sommets()) + "," + str(self.__nb_sommets) + ")")
       
       
       #ne_liste = [1,5,9,63]


#####Class point
point_un = p.Point(2,3)

print(point_un.x)
print(point_un.y)
print(point_un.r())
print(point_un.t())
print(point_un.__str__()) 

point_deux =p.Point(0,0)

print(point_un.__eq__(point_deux))  
print(point_un.homothetie(2))    
print(point_un.translation(1,2))  
print(point_un.rotation(2))

print("##########Class ligne pol###############")
#####Class ligne pol
liste_point=[point_un,point_deux,point_un]

ligne=LignePol(liste_point)


print("récuperer  sommet",ligne.get_sommet(0))

#print("set les sommets",ligne.set_sommet(0,8)) #car on a mis qu'une seule valeur 

print("homothetie",ligne.homothetie(2))

print("translation",ligne.translation(1,2))

print("rotation",ligne.rotation(8))



