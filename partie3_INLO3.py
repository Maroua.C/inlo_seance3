import math as m

class Point:
    "Un point doit avoir deux attributs num´eriques x, y"
    def __init__(self, x, y):
        #ici on les declare avec des __ devant pour eviter confusion avec les methode declarer apres
        self.x = x
        self.y = y
       
    def x(self):
        """
            renvoient les coordonnées cartésiennes du point x
            C'est un getter meme principe pour y
        """
        return self.x
   
    def y(self):
        return self.y
   
    def r(self):
        """
            on va utiler les getter defini avant comme ca on va pas utiliser directement
            les variable du construteur d'ou l'interet des getter
            coordonne polaire
        """
        return m.sqrt(self.x **2) + m.sqrt(self.y **2)
   
    def t(self):
        """
        coordonne polaire
        """

        
        return m.atan2(self.x,self.y)
   
    def __str__(self):
        return "({},{})".format(str(self.x),str(self.y))
     
       
    def __eq__(self,another_point):
        """
            self et another point sont des objets
            a et b représentent-ils deux points égaux ?
        """
        
        return 'ordonnee',self.y == another_point.y, 'abscisse',self.x ==  another_point.x
        
        

    def homothetie(self,k):
        """
            applique au point une homothétie de centre (0, 0) et de rapport k (k est un
            flottant) ; pour cela, il suffit de remplacer (x, y) par (k × x,k × y)
        """
        self.x = self.x * k 
        self.y = self.y * k
        return "homothetie : ({},{})".format(str(self.x),str(self.y))
       
    def translation(self,dx,dy):
        """
            applique au point une translation de vecteur (dx, dy) ; cela consiste à
            remplacer (x, y) par (x + dx, y + dy).
        """

        self.x = self.x + dx
        self.y = self.y + dy
        return "translation : ({},{})".format(str(self.x),str(self.y))

       
    def rotation(self,a):
        """  
            applique au point une rotation de centre (0, 0) et d’angle a
        """
        #variable local
        t=self.t()
        r=self.r() 
        
        x = r*(m.cos(t)) + a 
        y = r*(m.sin(t)) +a
        return "rotation : ({},{})".format(str(x),str(y))


class Rectangle:
	def __init__(self, x1, y1, x0, y0):
		self.coin_no = Point(x1, y1)
		self.coin_se = Point(x0, y0)
		
	def __str__(self):
		return '[ {} ; {} ]'.format(str(self.coin_no) ,str(self.coin_se))
		


from copy import copy,deepcopy

if __name__ == '__main__':
	"""
	point_un = Point(2,8)
	point_deux =Point(0,0)


	#####Class rectangle
	liste_point=[point_un,point_deux]

	Un_rectangle=Rectangle(point_un.x,point_un.y,point_deux.x,point_deux.y)
	print(Un_rectangle)
	"""
	
##############
#PARTIE##3B###
##############


	r1=Rectangle(1,2,3,4)
	r2 = r1

	
	print( "r1: " + str(r1) + ", r2: " + str(r2) )
	r1.coin_no = Point(0, 0)
	print( "r1: "+ str(r1) + ", r2:" + str(r2) )
	r1.coin_se.homothetie(2) 
	print( "r1: " + str(r1) + ", r2: " + str(r2))
	
	


	#~ On aurait pu penser que l’expression « ​r2 = r1​ » affectait à ​r2 une duplication de ​r1​. 
	#~ Qu’est-ce que l’exécution du code précédent montre ? 
	#~ Vous répondrez en commentaire dans votre fichier.
	
	#~ Cette expression ne fait pas une affectation mais va provoquer le fait que r2 va pointer au même endroit que r1
	#~ Chaque modification r1 sera effectuer sur r2 d'ou le r1.coin_no(0,0) et le homethetie qui modifie r1 ET r2
	

	print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")		

##############
#PARTIE##3C###
##############

	r1=Rectangle(1,2,3,4)
	r2 = copy(r1)

	
	print( "r1: " + str(r1) + ", r2: " + str(r2) )
	r1.coin_no = Point(0, 0)
	print( "r1: "+ str(r1) + ", r2:" + str(r2) )
	r1.coin_se.homothetie(2) 
	print( "r1: " + str(r1) + ", r2: " + str(r2))
	

	 #~ Exécutez à nouveau le test : que constatez-vous ?
	 
	 #~ Lorsque r1.coin_no est modifier r2.coin_no ne l'ai pas et lorsque que r1.coin_se est modifier en r1.coin_se.homothetie(2)
	 #~ r2.coin_se est également impacter
	 
	 #~ Cela s'explique par le fait que copy va copier r1 et crée un r2 clone de r1, r1 et r2 vont pointer sur les même valeurs mais 
	 #~ auront des no,se différents c'est pour cela que lorsqu'on modifie r1.coin_no en Point(0, 0)  r2.coin_no n'est pas impacter car on crée un new emplacement 
	 #~ mémoire et r1.coin_no va pointer vers celui la. 
	 #~ r1.coin_se et r2.coin_se sont modifier de facon semblable car on va juste effectuer une opération dans cette meme 
	 #~ emplacement mémoire (ou r1 et r2 pointe)


	print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")		

##############
#PARTIE##3D###
##############

	r1=Rectangle(1,2,3,4)
	r2 = deepcopy(r1)

	
	print( "r1: " + str(r1) + ", r2: " + str(r2) )
	r1.coin_no = Point(0, 0)
	print( "r1: "+ str(r1) + ", r2:" + str(r2) )
	r1.coin_se.homothetie(2) 
	print( "r1: " + str(r1) + ", r2: " + str(r2))
	

	#~ Que constatez-vous à présent ?
	
	#~ Toute les modifications faite sur r1 ne vont pas impacter r2.En effet avec deepcopy on va generer r2 clone de r1 mais qui vont pointer vers des valeurs différents 
	#~ (meme si les valeurs sont identifiques).r1 et r2 seront totalement independant l'un de l'autre niveau pointeur.
	




		
		
